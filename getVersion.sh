#!/bin/bash
#===============================================================================
#
#	Copyright (c) 2018 by the l0nax Software, Inc (Francesco Emanuel Bennici <benniciemanuel78@gmail.com>)
#
#          FILE: getVersion.sh
#
#   DESCRIPTION: Git Version Tool
#
#  REQUIREMENTS: git, bash, read, sed, help2man (only if you generate the man-page)
#         NOTES: If you run this Script in Docker, use a Image that Supports bash and the Command 'read'
#        AUTHOR: Francesco Emanuel Bennici <benniciemanuel78@gmail.com>
#
#  ORGANIZATION: l0nax Software, Inc
#       CREATED: 03/29/2018 01:54:02 AM
#
#       VERSION: 0.2 (PLEASE READ THE ISSUE [https://gitlab.com/l0nax/version_System/issues/1])
#
#================================
# Gedanken Stütze für die Weiterarbeit:
# $ git describe --tags
#   v0.0.5-1-g1ef24e5
#
#   v0.0.5  -- the last recent tag
#   1 -- number of commits done since that tag
#   g1ef24e5 -- the SHA1 hash of the current commit
#---------------------
#===============================================================================
# Version: 0.2 (PLEASE READ THE ISSUE [https://gitlab.com/l0nax/version_System/issues/1])
#
# This script published under MIT License
# https://en.wikipedia.org/wiki/MIT_License
#
# It's suggested to call this script from a "run script" build phase, not copy the script's contents there.
#
# CMake Example: https://gitlab.com/snippets/1706828
#
#
VERSION="0.2"
COPYRIGTH="Copyright (c) 2018 by the l0nax Software, Inc (Francesco Emanuel Bennici <benniciemanuel78@gmail.com>)"

## ==== START USAGE ====
# getVersion.sh is a Bash Script to get automated the Version of the Software.
# The Script will get the latest Tag of the git Repository and Parse the Tag Title.
# The Tag must be build in this form: vMAJOR.MINOR.PATCH-ADDITION (the '-ADDITION' is Optional!)
#
# Usage:
#       ./getVersion.sh [OPTIONS]
#
# Options:
#       -v, --version
#              Print the Version of the
#
#       -h, --help
#              Print this help Page
#
#       -m, --major
#              Print only Major Number
#
#       -i, --minor
#              Print only Major Number
#
#       -p, --patch
#              Print only Major Number
#
#       -b, --build
#              Print only Major Number
#
#       -r, --revision
#              Print only Major Number
#
#       -a, --addition
#              Print only Major Number
#
#       -n, --without-revision
#              Do not print the Revision Number
#
#       -d, --without-build
#              Do not print the Build Number
#
#       --revision-num [NUMBER]
#              Equal to --build-num only the revision number is filled up.
#              Default Value: false
#              Example:
#                     ./getVersion.sh --revision-num 4
#              Example Output:
#                     v1.0.4 (Build: 14 Revision: 0009)
#
#       -g, --generate
#              Generate Manpage of this File
#
#       FORMAT
#              To set the Format of the Version, create the Enviroiment Variable 'FORMAT',
#              and create your Version String.
#              Usable Variables in Format:
#                 MAJOR, MINOR, PATCH, BUILD, REVISION, ADDITION
#              Please add the Characters '[[[[' and ']]]]' around the ADDITION Variable,
#              so this Script can skip the Addition Variable if there is no in the Build.
#              Example:
#                     FORMAT="vMAJOR.MINOR.PATCH[[[[-ADDITION]]]] (Build: BUILD Revision: REVISION)"
#              Output (with Addition):
#                     v1.4.5-BETA (Build: 8 Revision: 26497)
#              Output (without Addition):
#                     v1.4.5 (Build: 16 Revision: 26787)
## ==== END USAGE ====

### Variables
# Colors
BLACK='\033[0;30m'
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
LIGHT_RED='\033[1;31m'
GREEN='\033[0;32m'
LIGHT_GREEN='\033[1;32m'
ORANGE='\033[0;33m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
LIGHT_BLUE='\033[1;34m'
PURPLE='\033[0;35m'
LIGHT_PURPLE='\033[1;35m'
CYAN='\033[0;36m'
LIGHT_CYAN='\033[1;36m'
LIGHT_GRAY='\033[0;37m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

### SECURITY CHECK
# unset all Enviroiment Variables used in this Script
unset OPT_MAJOR
unset OPT_MINOR
unset OPT_PATCH
unset OPT_BUILD
unset OPT_REVISION
unset OPT_LONGHASH
unset OPT_SHORTHASH
unset OPT_ADDITION
unset OPT_WITHOUTBUILD
unset OPT_WITHOUTREVISION
unset OPT_REVISION_NUM
unset OPT_BUILD_NUM
unset PRINT_LINE
unset SCRIPT_DIR
unset file
unset ADDITION
unset BUILD
unset REVISION

# Check if git is installed
dpkg -s git > /dev/null 2>&1
return=$?
if [ $return != 0 ]; then
	echo -e "${LIGHT_GRAY}[${RED}-${LIGHT_GRAY}]${NC} Please install first git e. g. with 'apt-get install git'"
	exit 1
fi


### FUNCTION
usage () {
	SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	file=$SCRIPT_DIR/getVersion.sh

	# option that says if we can print the Line or not
	PRINT_LINE=

	while IFS= read line
	do
		# check if Line is Start Usage
		if [ "$line" == "## ==== START USAGE ====" ]; then
			PRINT_LINE=true
			continue
		fi

		# check if Line is End Usage
		if [ "$line" == "## ==== END USAGE ====" ]; then
			break
		fi

		# check if we can Print the Line
		if [ "$PRINT_LINE" == "true" ]; then
			# print $line and remove '#' from String
			echo "${line:2}"
		fi
	done < "$file"
}

### CODE
# Get a svn-like revision number that keeps increasing with every commit.
REV=$(git log --pretty=format:'' | wc -l | sed 's/[ \t]//g')

# Getting the current branch
GITBRANCH=$(git branch | grep "*" | sed -e 's/^* //')

# full build hash
GITHASH=$(git rev-parse HEAD)

# commonly used short hash
GITHASHSHORT=$(git rev-parse --short HEAD)

# parsing tags to build the Application Version in the form of v<MAJOR>.<MINOR>.<PATCH>-<ADDITION>
# Parts of the version number that are missing are substituted with zeros.
NEAREST=$(git describe --tags $(git rev-list --tags --max-count=1))

################################
##	    Option Variables      ##
################################
## Informations ##
# To check if a Variable is true use this Code:
#    if [ $VARIABLE -eq "true" ]; then
#        echo "Variable is True!"
#    else
#        echo "Variable is False!"
#    fi
# A Variable is only then True if the Variable contains any String,
# if the Variable contains nothing it will be false!
# Here is a Example:
#        test_var=false
#        if [ $test_var -eq "true" ]; then echo "test_var is True!"; else echo "test_var is False!"; fi
# The Result of this Test will be False, because the Variable is Empty!

## Variables ##
OPT_MAJOR=false
OPT_MINOR=false
OPT_PATCH=false
OPT_BUILD=false
OPT_REVISION=false
OPT_LONGHASH=false
OPT_SHORTHASH=false
OPT_ADDITION=false

OPT_WITHOUTBUILD=false
OPT_WITHOUTREVISION=false

OPT_REVISION_NUM=
######
################################

## check Options
if [ ! $# -eq 0 ]; then
	while [ "$1" != "" ]; do
		case "$1" in
			-h|--help)
				usage
				exit
				;;
			-v|--version)
				echo "getVersion.sh $VERSION"
				echo
				# print Copyrigth
				echo $COPYRIGTH
				echo

				# print Author(s)
				echo "Written by Francesco Emanuel Bennici <benniciemanuel78@gmail.com>"
				exit
				;;

			-m|--major)
				OPT_MAJOR=true
				break
				;;

			-i|--minor)
				OPT_MINOR=true
				break
				;;

			-p|--patch)
				OPT_PATCH=true
				break
				;;

			-b|--build)
				OPT_BUILD=true
				break
				;;

			-r|--revision)
				OPT_REVISION=true
				break
				;;

			-n|--without-revision)
				OPT_WITHOUTREVISION=true
				;;

			-d|--without-build)
				OPT_WITHOUTBUILD=true
				;;

			--revision-num)
				shift
				if [ ! $1 == "" ]; then
					OPT_REVISION_NUM=$1
				fi
				;;

			-g|--generate)
				# Check if 'help2man' is installed
				dpkg -s help2man > /dev/null 2>&1
				return=$?
				if [ $return != 0 ]; then
					# try to Install 'help2man'
					{
					apt-get install help2man -y
					} > /dev/null 2>&1

					return=$?

					# check if Package installation failed
					if [ $return != 0 ]; then
						echo -e "${LIGHT_GRAY}[${RED}-${LIGHT_GRAY}]${NC} Please install first git e. g. with 'apt-get install help2man'"
						exit 1
					fi
				fi



				# Generate Manpage
				SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
				file=$SCRIPT_DIR/getVersion.sh

				chmod +x $file
				help2man $file > getVersion_man

				echo -e "${LIGHT_GRAY}[${GREEN}-${LIGHT_GRAY}]${NC} You can now run 'man ./getVersion_man' to see the Manpage of 'getVersion.sh'"
				;;

		esac
		shift # argument $2 becomes $1, $3 becomes $2 and so on..
	done
fi

MAJOR="0"
MINOR="0"
PATCH="0"

if [ -z $NEAREST ]; then
	echo "ERROR"
else
	MAJOR=$(echo $NEAREST | cut -d "v" -f2 | cut -d "." -f1)
	if [ -z $MAJOR ]; then
		MAJOR="0"
	fi

	MINOR=$(echo $NEAREST | cut -d "v" -f2 | cut -d "." -f2)
	if [ -z $MINOR ]; then
		MINOR="0"
	fi

	# get Patch & Build
	PATCH=$(echo $NEAREST | cut -d "v" -f2 | cut -d "." -f3 | cut -d "-" -f1)
	BUILD=$(git log -n 1 | head -n 1 | sed -e 's/^commit //' | head -c 8)

    ## Check if Tag has Addition
    ## (See Issue: https://gitlab.com/l0nax/version_System/issues/2)
    ADDITION=$(git describe --tags | cut -d "-" -f2)
		# add '-' to ADDITION if custom Format was not set by the User
		if [ -z "${FORMAT}" ]; then
			ADDITION="-$ADDITION"
		fi

    if [[ ! $ADDITION = "" ]]; then
        # Tag does not contains Version Addition
        unset ADDITION
    fi
fi

# check if 'FORMAT' flag was set
if [ -z "${FORMAT}" ]; then
	# check if Addition is in Version
	if [ -z "$ADDITION" ]; then # ADDITION is Empty
		# Replace [[[[*ADDITION]]]] with nothing
		FORMAT=$(echo $FORMAT | sed -e 's/\[\[\[\[.*\]\]\]\]//g')
	else # Addition was set
		FORMAT=$(echo $FORMAT | sed -e 's/\[\[\[\[\(.*\)\]\]\]\]/\1/')
	fi

	# Replace 'MAJOR'
	FORMAT=$(echo $FORMAT | sed -e 's/MAJOR/$MAJOR/g')

	# Replace 'MINOR'
	FORMAT=$(echo $FORMAT | sed -e 's/MINOR/$MINOR/g')

	# Replace 'PATCH'
	FORMAT=$(echo $FORMAT | sed -e 's/PATCH/$PATCH/g')

	# Replace 'BUILD
	FORMAT=$(echo $FORMAT | sed -e 's/BUILD/$BUILD/g')

	# Replace 'REVISION'
	FORMAT=$(echo $FORMAT | sed -e 's/REVISION/$REV/g')

	# Replace 'ADDITION'
	FORMAT=$(echo $FORMAT | sed -e 's/ADDITION/$ADDITION/g')
fi

# check if Revision is not set
if [ ! "$OPT_REVISION_NUM" = "" ]; then
    for (( i=1; i<=$OPT_REVISION_NUM; i++ )); do
        REVISION="0$REVISION"
    done
fi

## Append $REV to $REVISION
REVISION="$REVISION$REV"

#######################################
##          Section 'Print'          ##
#######################################
# check if option 'Without Build' is set ONLY
if [[ "$OPT_WITHOUTBUILD" = "true" ]] && [[ ! "$OPT_WITHOUTREVISION" = "true" ]]; then
	echo -n "$MAJOR.$MINOR.$PATCH$ADDITION (Revision: $REVISION)"
	exit 0
fi

# check if option 'Without Revision' is set
# (and check if option 'Without Build is set')
if [ "$OPT_WITHOUTREVISION" = "true" ]; then
	if [ "$OPT_WITHOUTBUILD" = "true" ]; then
		echo -n "$MAJOR.$MINOR.$PATCH$ADDITION"
		exit 0
	else
		echo -n "$MAJOR.$MINOR.$PATCH$ADDITION (Build: $BUILD)"
		exit 0
	fi
fi

## print the Default Version String
echo -n "$MAJOR.$MINOR.$PATCH$ADDITION (Build: $BUILD Revision: $REVISION)"
